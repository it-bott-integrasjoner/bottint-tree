from typing import Optional, List

import pytest

from bottint_tree import (
    DuplicateItem,
    LoopDetected,
    MissingRoot,
    ParentNotFound,
    Tree,
    build_forest,
    __version__,
)


def test_version():
    assert __version__ == "0.5.1"


def test_poetry_version():
    import importlib.metadata

    version = importlib.metadata.version("bottint-tree")

    assert version == __version__


class Node:
    id: Optional[int]
    parent_id: Optional[int]

    def __init__(self, id=None, parent_id=None):
        self.id = id
        self.parent_id = parent_id

    def __repr__(self) -> str:
        return f"Node(id={repr(self.id)}, parent_id={repr(self.parent_id)})"

    def __eq__(self, o) -> bool:
        return o.id == self.id and o.parent_id == self.parent_id

    def __str__(self) -> str:
        return f"id={self.id} parent_id={self.parent_id}"

    def __lt__(self, other) -> bool:
        return self.id < other.id


def _build_forest(items: List[Node]):
    return build_forest(items, "id", "parent_id")


@pytest.fixture
def node_1():
    return Node(id=1, parent_id=None)


@pytest.fixture
def node_2(node_1):
    return Node(id=2, parent_id=node_1.id)


@pytest.fixture
def node_3(node_1):
    return Node(id=3, parent_id=node_1.id)


@pytest.fixture
def node_4(node_2):
    return Node(id=4, parent_id=node_2.id)


@pytest.fixture
def node_5(node_4):
    return Node(id=5, parent_id=node_4.id)


@pytest.fixture
def node_x_1():
    return Node(id=10001, parent_id=None)


@pytest.fixture
def node_x_2(node_x_1):
    return Node(id=10002, parent_id=node_x_1.id)


@pytest.fixture
def node_with_id_equals_parent_id():
    return Node(id=20001, parent_id=20001)


@pytest.fixture
def nodes(
    node_1,
    node_2,
    node_3,
    node_4,
    node_5,
):
    return [
        node_1,
        node_2,
        node_3,
        node_4,
        node_5,
    ]


@pytest.fixture
def nodes_x(
    node_x_1,
    node_x_2,
):
    return [
        node_x_1,
        node_x_2,
    ]


@pytest.fixture
def tree_1(
    node_1,
    node_2,
    node_3,
    node_4,
    node_5,
):
    t_5 = Tree(data=node_5, id_key="id", children=[])
    t_4 = Tree(data=node_4, id_key="id", children=[t_5])
    t_3 = Tree(data=node_3, id_key="id", children=[])
    t_2 = Tree(data=node_2, id_key="id", children=[t_4])
    t_1 = Tree(data=node_1, id_key="id", children=[t_2, t_3])

    t_2.parent = t_1
    t_3.parent = t_1
    t_4.parent = t_2
    t_5.parent = t_4

    return [t_1, t_2, t_3, t_4, t_5]


@pytest.fixture
def tree_x1(
    node_x_1,
    node_x_2,
):
    t_x_2 = Tree(data=node_x_2, id_key="id", children=[])
    t_x_1 = Tree(data=node_x_1, id_key="id", children=[t_x_2])
    t_x_2.parent = t_x_1

    return [t_x_1, t_x_2]


@pytest.fixture
def tree_with_loop(
    node_x_1,
    node_x_2,
):
    t_x_2 = Tree(data=node_x_2, id_key="id", children=[])
    t_x_1 = Tree(data=node_x_1, id_key="id", children=[t_x_2])
    t_x_2.parent = t_x_1
    t_x_1.parent = t_x_2

    return [t_x_1, t_x_2]


@pytest.fixture
def forest(tree_1, tree_x1):
    return {x.id: x for x in [*tree_1, *tree_x1]}


def test_build_forest_loop_parent_equals_key(
    node_1,
    node_with_id_equals_parent_id,
):
    items = [node_1, node_with_id_equals_parent_id]

    with pytest.raises(LoopDetected, match=r"^Item is it's own parent$"):
        _build_forest(items)


def test_build_forest_duplicate_item(
    node_1,
    node_2,
):
    items = [node_1, node_2, node_1]

    with pytest.raises(DuplicateItem, match=r"^1$"):
        _build_forest(items)


def test_build_forest_missing_root(node_2, node_3):
    items = [node_2, node_3]

    with pytest.raises(MissingRoot):
        _build_forest(items)


def test_build_forest_no_id():
    items = [Node(id=None, parent_id=None)]

    with pytest.raises(ValueError, match=r"^`item.id` can't be None$"):
        _build_forest(items)


def test_build_forest_parent_not_found(
    node_1,
    node_2,
    node_3,
    node_5,
):
    items = [
        node_1,
        node_2,
        node_3,
        node_5,
    ]

    with pytest.raises(ParentNotFound):
        _build_forest(items)


def test_build_forest(nodes, nodes_x, forest):
    x = _build_forest([*nodes, *nodes_x])

    assert x == forest


def test_tree_iter(tree_1):
    xs = list(sorted(tree_1[0], key=lambda x: x.id))

    assert xs == tree_1


def test_tree_values(tree_1, nodes):
    xs = list(sorted(tree_1[0].values(), key=lambda x: x.id))

    assert xs == nodes


def test_depth(tree_1):
    assert tree_1[0].depth == 1
    assert tree_1[1].depth == 2


def test_depth_throws_loop_detected(tree_with_loop):
    with pytest.raises(LoopDetected):
        tree_with_loop[0].depth  # noqa


def test___repr__(tree_1):
    assert eval(repr(tree_1)) == tree_1


def test_build_forest_with_callables(nodes):
    class NodeWithStrKey(Node):
        def __init__(self, id: Optional[int] = None, parent_id: Optional[int] = None):
            super().__init__(id, parent_id)
            self.external_keys: List[str] = [str(id)] if id is not None else []

    nodes = [NodeWithStrKey(x.id, x.parent_id) for x in nodes]
    forest = build_forest(
        nodes,
        id_key=lambda x: x.external_keys[0] if x.external_keys else None,
        parent_key=lambda x: str(x.parent_id) if x.parent_id else None,
    )

    assert sorted(forest["1"].values()) == sorted(nodes)
