from __future__ import annotations

from itertools import chain
from typing import (
    List,
    Iterable,
    Generic,
    TypeVar,
    Dict,
    Iterator,
    Any,
    Optional,
    Union,
    Callable,
)

__version__ = "0.5.1"

T = TypeVar("T")


def get_value(key: Union[str, Callable[[T], Any]], data: T) -> Any:
    if isinstance(key, str):
        return getattr(data, key)
    return key(data)


class Tree(Generic[T]):
    """Generic tree."""

    def __init__(
        self,
        data: T,
        id_key: Union[str, Callable[[T], Any]],
        children: List[Tree[T]],
        parent: Optional[Tree[T]] = None,
    ):
        assert data is not None
        assert isinstance(id_key, str) or callable(id_key)
        self.data = data
        self.id_key = id_key
        if parent is not None:
            assert isinstance(parent, Tree)
        for child in children:
            assert isinstance(child, Tree)
        self.parent: Optional[Tree[T]] = parent
        self.children = children

    def __iter__(self) -> Iterator[Tree[T]]:
        yield self
        yield from chain(*map(iter, self.children))  # type: ignore[arg-type]

    def __repr__(self):
        children = ", ".join(list(map(repr, self.children)))
        return f"Tree(data={repr(self.data)}, id_key={repr(self.id_key)}, children=[{children}])"

    def __eq__(self, other):
        return all(
            [
                self.id_key == other.id_key,
                self.data == other.data,
                self.children == other.children,
            ]
        )

    def values(self) -> Iterator[T]:
        for x in self:
            yield x.data

    @property
    def id(self) -> Any:
        return get_value(key=self.id_key, data=self.data)

    @property
    def depth(self) -> int:
        """Root node has depth 1"""
        visited = set()

        def go(x: Tree[T]):
            if x.id in visited:
                raise LoopDetected
            if x.parent is None:
                return 0
            visited.add(x.id)
            return 1 + go(x.parent)

        return 1 + go(self)


class InvalidTree(Exception):
    pass


class LoopDetected(InvalidTree):
    pass


class DuplicateItem(InvalidTree):
    pass


class MissingRoot(InvalidTree):
    pass


class ParentNotFound(InvalidTree):
    pass


def build_forest(
    items: Iterable[T],
    id_key: Union[str, Callable[[T], Any]],
    parent_key: Union[str, Callable[[T], Any]],
    check_root: bool = True,
    check_parent: bool = True,
) -> Dict[Any, Tree[T]]:
    """

    Args:
        items:        Collection to build forest from
        id_key:       Name of id property of T
        parent_key:   Name of parent property of T
        check_root:   Check if an element with parent == None is found in items. Raise MissingRoot if it fails
        check_parent: Check if parent is found in items. Raise ParentNotFound if it fails

    Returns:
        A dictionary of all elements, keyed by property with name id_key

    Checks for loops and duplicate items and raises LoopDetected or DuplicateItem if that is the case
    """
    graph_by_id: Dict[Any, Tree[T]] = {}
    children: Dict[Any, List[Tree[T]]] = {}
    visited = set()
    has_root = False

    for item in items:
        id_ = get_value(key=id_key, data=item)
        if id_ is None:
            raise ValueError(f"`item.{id_key}` can't be None")
        if id_ in visited:
            raise DuplicateItem(str(id_))
        visited.add(id_)
        parent = get_value(key=parent_key, data=item)
        if parent == id_:
            raise LoopDetected("Item is it's own parent")
        if id_ not in children:
            children[id_] = []
        node = Tree(data=item, id_key=id_key, children=children[id_])
        graph_by_id[id_] = node
        if parent is not None:
            if parent not in children:
                children[parent] = []
            children[parent].append(node)
        else:
            has_root = True

    if check_root and not has_root:
        raise MissingRoot

    for t in graph_by_id.values():
        parent = get_value(key=parent_key, data=t.data)
        if parent is not None:
            if check_parent and parent not in graph_by_id:
                raise ParentNotFound(str(parent))
            t.parent = graph_by_id.get(parent)

    return graph_by_id
