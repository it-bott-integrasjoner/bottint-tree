[![pipeline status](https://git.app.uib.no/it-bott-integrasjoner/bottint-tree/badges/master/pipeline.svg)](https://git.app.uib.no/it-bott-integrasjoner/bottint-tree/-/commits/master)
[![coverage report](https://git.app.uib.no/it-bott-integrasjoner/bottint-tree/badges/master/coverage.svg)](https://git.app.uib.no/it-bott-integrasjoner/bottint-tree/-/commits/master)

# bottint-tree

A basic generic tree structure

## Development

```shell
poetry shell
poetry install
pre-commit install
```
